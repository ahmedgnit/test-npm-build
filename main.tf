provider "aws" {
}

terraform {
  required_version = ">= 0.12.8"
}

# frontend

resource "aws_s3_bucket" "frontend_bucket" {
  force_destroy = "true"
  website {
    index_document = "index.html"
  }
}
#"${path.module}/oidc-thumbprint.sh" 
resource "null_resource" "build" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    command = <<-EOF
      npm install
      npm run build
    EOF
 
    #environment = var.environment_variables
  }
}

data "archive_file" "source_zip" {
  depends_on = [null_resource.build]
  type        = "zip"
  source_dir  = "../build"
  output_path = "dist/source.zip"
}

locals {
  mime_type_mappings = {
    html = "text/html",
    js   = "text/javacript",
    css  = "text/css"
  }
}

resource "aws_s3_bucket_object" "frontend_object" {
  for_each = fileset("${data.archive_file.source_zip.output_path}", "*")

  key          = each.value
  source       = "${data.archive_file.source_zip.output_path}/${each.value}"
  bucket       = aws_s3_bucket.frontend_bucket.bucket
  etag         = filemd5("${data.archive_file.source_zip.output_path}/${data.archive_file.source_zip}/${each.value}")
  content_type = lookup(local.mime_type_mappings, concat(regexall("\\.([^\\.]*)$", each.value), [[""]])[0][0], "application/octet-stream")

  // if you prefer an error if the MIME type is missing from mime_type_mappings
  // content_type = local.mime_type_mappings[concat(regexall("\\.([^\\.]*)$", each.value), [[""]])[0][0]]
}

# Boilerplate for the bucket

resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.frontend_bucket.id
  policy = data.aws_iam_policy_document.default.json
}

data "aws_iam_policy_document" "default" {
  statement {
    actions = ["s3:GetObject"]

    resources = ["${aws_s3_bucket.frontend_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

output "frontend_url" {
  value = "${aws_s3_bucket.frontend_bucket.website_endpoint}"
}
